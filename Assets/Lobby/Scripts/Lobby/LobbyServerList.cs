﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Prototype.NetworkLobby
{
    public class LobbyServerList : MonoBehaviour
    {
        public LobbyManager lobbyManager;

        public RectTransform serverListRect;
        public GameObject serverEntryPrefab;
        public GameObject noServerFound;

        protected int currentPage = 0;
        protected int previousPage = 0;

        static Color OddServerColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        static Color EvenServerColor = new Color(.94f, .94f, .94f, 1.0f);

        public EthereumService eth;
        public RectTransform lobbyPanel;
        public TokenNat nat;


        void OnEnable()
        {
            currentPage = 0;
            previousPage = 0;

            foreach (Transform t in serverListRect)
                Destroy(t.gameObject);

            noServerFound.SetActive(false);

            StartCoroutine(GettingData());


            //RequestPage(0);
        }



        IEnumerator GettingData()
        {

            StartCoroutine(eth.GetData());

            yield return new WaitUntil(() => eth.Obtenidos == true);
            Debug.Log("data=" + eth.data);

            if (string.IsNullOrEmpty(eth.data) == true)
            {
                noServerFound.SetActive(true);
                yield break;

            }

            noServerFound.SetActive(false);
            foreach (Transform t in serverListRect)
                Destroy(t.gameObject);


            string[] output = eth.data.Split(Convert.ToChar(eth._DELIMITEREND));
            
            for (int i = 0; i < output.Length - 1; i++)
            {
                GameObject o = Instantiate(serverEntryPrefab) as GameObject;

                string nombre = output[i].Split(Convert.ToChar(eth._DELIMITERBTW))[0];
                string ip = output[i].Split(Convert.ToChar(eth._DELIMITERBTW))[1];

                o.GetComponent<LobbyServerEntry>().serverInfoText.text = nombre;

                if (i % 2 == 0) { o.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f); }
                else { o.GetComponent<Image>().color = new Color(.94f, .94f, .94f, 1.0f); }

                o.GetComponentInChildren<Button>().onClick.AddListener(delegate { JoinGame(nombre, ip); });

                o.transform.SetParent(serverListRect, false);
            }

        }



        public void JoinGame(string nameServer, string ipAddress)
        {
            Debug.Log("join" + ipAddress);

            lobbyManager.ChangeTo(lobbyPanel);

            lobbyManager.networkAddress = ipAddress;
            lobbyManager.networkPort = nat.publicPort;
            lobbyManager.StartClient();

            lobbyManager.backDelegate = lobbyManager.StopClientClbk;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Connecting...", lobbyManager.networkAddress);

        }




        public void OnGUIMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
        {
            if (matches.Count == 0)
            {
                if (currentPage == 0)
                {
                    noServerFound.SetActive(true);
                }

                currentPage = previousPage;

                return;
            }

            noServerFound.SetActive(false);
            foreach (Transform t in serverListRect)
                Destroy(t.gameObject);

            for (int i = 0; i < matches.Count; ++i)
            {
                GameObject o = Instantiate(serverEntryPrefab) as GameObject;

                o.GetComponent<LobbyServerEntry>().Populate(matches[i], lobbyManager, (i % 2 == 0) ? OddServerColor : EvenServerColor);

                o.transform.SetParent(serverListRect, false);
            }
        }

        public void ChangePage(int dir)
        {
            int newPage = Mathf.Max(0, currentPage + dir);

            //if we have no server currently displayed, need we need to refresh page0 first instead of trying to fetch any other page
            if (noServerFound.activeSelf)
                newPage = 0;

            RequestPage(newPage);
        }

        public void RequestPage(int page)
        {
            previousPage = currentPage;
            currentPage = page;
            lobbyManager.matchMaker.ListMatches(page, 6, "", true, 0, 0, OnGUIMatchList);
        }
    }
}