using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.Networking;

namespace Prototype.NetworkLobby
{
    //Main menu, mainly only a bunch of callback called by the UI (setup throught the Inspector)
    public class LobbyMainMenu : MonoBehaviour 
    {
        public LobbyManager lobbyManager;

        public RectTransform lobbyServerList;
        public RectTransform lobbyPanel;

        public InputField ipInput;
        public InputField matchNameInput;

        public EthereumService eth;
        public TokenNat nat;
        public LobbyPlayerList lobbyPList;

        //public NetworkClient client = new NetworkClient();

        public GameObject cl;

        public void OnEnable()
        {
            lobbyManager.topPanel.ToggleVisibility(true);

            ipInput.onEndEdit.RemoveAllListeners();
            ipInput.onEndEdit.AddListener(onEndEditIP);

            matchNameInput.onEndEdit.RemoveAllListeners();
            matchNameInput.onEndEdit.AddListener(onEndEditGameName);
        }

        public void OnClickHost()
        {


            Debug.Log("123");
            lobbyManager.networkAddress = nat.privateIPAddress.ToString();
            lobbyManager.networkPort = nat.privatePort;

            lobbyManager.statusInfo.text = "Play and Host";
            lobbyManager.hostInfo.text = nat.publicIPAddress.ToString();

            var clien = lobbyManager.StartHost();
            
            Debug.Log("init client/host?" + clien.isConnected);

        }

        public void OnClickJoin()
        {

            lobbyManager.networkAddress = ipInput.text;
            lobbyManager.networkPort = nat.privatePort;

            var clien = lobbyManager.StartClient();
            
            Debug.Log("isconnected?" + clien.isConnected);

            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Connecting...", ipInput.text);

            lobbyManager.ChangeTo(lobbyPanel);
        }

        public void OnClickDedicated()
        {
            lobbyManager.ChangeTo(null);

            lobbyManager.networkAddress = nat.privateIPAddress.ToString();
            lobbyManager.networkPort = nat.privatePort;

            lobbyManager.StartServer();

            lobbyManager.statusInfo.text = "Dedicated Server";
            lobbyManager.hostInfo.text = nat.publicIPAddress.ToString();

            lobbyManager.backDelegate = lobbyManager.StopServerClbk;

          
        }

        public void OnClickCreateMatchmakingGame()
        {
            //        lobbyManager.StartMatchMaker();
            //        lobbyManager.matchMaker.CreateMatch(
            //            matchNameInput.text,
            //            (uint)lobbyManager.maxPlayers,
            //            true,
            //"", "", "", 0, 0,
            //lobbyManager.OnMatchCreate);


            nat.nameMatch = matchNameInput.text;

            lobbyManager.statusInfo.text = "Matchmaker Host";
            lobbyManager.hostInfo.text = nat.publicIPAddress.ToString();

            lobbyManager.networkAddress = nat.privateIPAddress.ToString();
            lobbyManager.networkPort = nat.privatePort;

            lobbyManager.StartHost();
            

            lobbyPList.lanzarSetServerEthereum();

            lobbyManager.DisplayIsConnecting();
            



        }

        public void OnClickOpenServerList()
        {
            //lobbyManager.StartMatchMaker();
            lobbyManager.backDelegate = lobbyManager.SimpleBackClbk;
            lobbyManager.ChangeTo(lobbyServerList);
        }

        void onEndEditIP(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickJoin();
            }
        }

        void onEndEditGameName(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickCreateMatchmakingGame();
            }
        }

    }
}
