﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.Hex.HexTypes;
using Nethereum.Contracts;
using System;

public class EthereumService : MonoBehaviour
{
    
    private string accountAddress;
    private string accountPrivateKey = "0xa704d5043b3aa70e81607d0f8e6388824d0329a45cb78daeaf54cd93b9efdcc1";
    
    private string _url = "https://kovan.infura.io";

    public static string ABI = @"[{""constant"":false,""inputs"":[{""name"":""server"",""type"":""bytes32""}],""name"":""SetServer"",""outputs"":[{""name"":"""",""type"":""bool""}],""payable"":false,""stateMutability"":""nonpayable"",""type"":""function""},{""constant"":true,""inputs"":[],""name"":""GetSizes"",""outputs"":[{""name"":"""",""type"":""uint256[]""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""constant"":false,""inputs"":[{""name"":""server"",""type"":""bytes32""}],""name"":""KillServer"",""outputs"":[{""name"":"""",""type"":""bool""}],""payable"":false,""stateMutability"":""nonpayable"",""type"":""function""},{""constant"":false,""inputs"":[{""name"":""server"",""type"":""bytes32""}],""name"":""SetSizePlusOne"",""outputs"":[{""name"":"""",""type"":""uint256""}],""payable"":false,""stateMutability"":""nonpayable"",""type"":""function""},{""constant"":true,""inputs"":[{""name"":"""",""type"":""uint256""}],""name"":""currentsize"",""outputs"":[{""name"":"""",""type"":""uint256""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""constant"":true,""inputs"":[{""name"":"""",""type"":""uint256""}],""name"":""servidores"",""outputs"":[{""name"":"""",""type"":""bytes32""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""constant"":true,""inputs"":[],""name"":""GetServers"",""outputs"":[{""name"":"""",""type"":""bytes32[]""}],""payable"":false,""stateMutability"":""view"",""type"":""function""}]";
    public static string byteCode = @"60606040526004600255341561001457600080fd5b61059e806100236000396000f3006060604052600436106100825763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166319eca03e8114610087578063678cced0146100b15780636945fe7a14610117578063956011bc1461012d578063cbdad2b914610155578063dfdc69031461016b578063e30a316414610181575b600080fd5b341561009257600080fd5b61009d600435610194565b604051901515815260200160405180910390f35b34156100bc57600080fd5b6100c46101e0565b60405160208082528190810183818151815260200191508051906020019060200280838360005b838110156101035780820151838201526020016100eb565b505050509050019250505060405180910390f35b341561012257600080fd5b61009d60043561023f565b341561013857600080fd5b61014360043561038a565b60405190815260200160405180910390f35b341561016057600080fd5b61014360043561045e565b341561017657600080fd5b61014360043561047d565b341561018c57600080fd5b6100c461048b565b600080548190600181016101a88382610519565b506000918252602090912001829055600180548082016101c88382610519565b50600091825260209091206001910181905592915050565b6101e8610542565b600180548060200260200160405190810160405280929190818152602001828054801561023457602002820191906000526020600020905b815481526020019060010190808311610220575b505050505090505b90565b600080805b60005482101561037e5761027260008381548110151561026057fe5b906000526020600020900154856104e9565b1515600114156103735750805b6000546000190181101561030457600080546001830190811061029e57fe5b9060005260206000209001546000828154811015156102b957fe5b600091825260209091200155600180548282019081106102d557fe5b9060005260206000209001546001828154811015156102f057fe5b60009182526020909120015560010161027f565b60008054600019810190811061031657fe5b60009182526020822001819055546001805490916000190190811061033757fe5b60009182526020822001819055805490610355906000198301610519565b506001805490610369906000198301610519565b5060019250610383565b600190910190610244565b600092505b5050919050565b6000805b600054811015610453576103bc6000828154811015156103aa57fe5b906000526020600020900154846104e9565b15156001141561044b5760025460018054839081106103d757fe5b90600052602060002090015410156104425760018054829081106103f757fe5b90600052602060002090015460010160018281548110151561041557fe5b600091825260209091200155600180548290811061042f57fe5b9060005260206000209001549150610458565b60009150610458565b60010161038e565b600091505b50919050565b600180548290811061046c57fe5b600091825260209091200154905081565b600080548290811061046c57fe5b610493610542565b600080548060200260200160405190810160405280929190818152602001828054801561023457602002820191906000526020600020905b815481526001909101906020018083116104cb575050505050905090565b60008160405190815260200160405190819003902083604051908152602001604051908190039020149392505050565b81548183558181151161053d5760008381526020902061053d918101908301610554565b505050565b60206040519081016040526000815290565b61023c91905b8082111561056e576000815560010161055a565b50905600a165627a7a72305820f67b60b45f647bc4d419f829bc5e06075554466e049ec12ea8a4e589cc9aa2060029";
    private static string contractAddress = "0x8b37d4756a25c4afd8b7d83f7b0fc27ebd5fcdf8";

    private Contract contract;

    private const ushort _MAXLENSTRING = 31; //bytes32 en sol
    public string _DELIMITERBTW = ":";
    
    public string _DELIMITEREND = "/";

    private DeployContractTransactionBuilder contractTransactionBuilder = new DeployContractTransactionBuilder();

    [HideInInspector]
    public bool Obtenidos = false;
    [HideInInspector]
    public string data = "";

    // Use this for initialization
    void Start()
    {

        DontDestroyOnLoad(this.gameObject);
        try
        {
            accountAddress = Nethereum.Signer.EthECKey.GetPublicAddress(accountPrivateKey);
            Debug.Log("Imported account SUCCESS" + accountAddress);
        }
        catch (Exception e)
        {
            Debug.Log("error=" + e);
        }

        contract = new Contract(null, ABI, contractAddress);


        //new contract
        //deployEthereumContract();


        //getBalanace();
       // StartCoroutine(GetData());
        //StartCoroutine(setServer("NOMBREMAT12345", "255.255.255.255"));

        //StartCoroutine(setServers("jose"));
        //StartCoroutine(KillServer("jose"));


    }


    public IEnumerator GetData()
    {
        Obtenidos = false;
        data = "";
        var Request = new EthCallUnityRequest(_url);
        var function = contract.GetFunction("GetServers");
        var callInput = function.CreateCallInput();
        yield return Request.SendRequest(callInput, Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());

        //0x
        //00000000000000000000000000000000
        //00000000000000000000000000000020
        //00000000000000000000000000000000
        //00000000000000000000000000000002
        //XXXXXXXX000000000000000000000000
        //00000000000000000000000000000000
        //XXXXXXXXXXXXXXXXXXXX000000000000
        //00000000000000000000000000000000

        data = HEX2ASCII(Request.Result.Substring(130).Replace("00", ""));
        //Debug.Log("data=" + data);
        //StartCoroutine(GetSizes());

        Obtenidos = true;

    }

    public IEnumerator GetSizes()
    {
       // Obtenidos = false;
        //data = "";
        var Request = new EthCallUnityRequest(_url);
        var function = contract.GetFunction("GetSizes");
        var callInput = function.CreateCallInput();
        yield return Request.SendRequest(callInput, Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        
        //0x
        //00000000000000000000000000000000
        //00000000000000000000000000000020
        //00000000000000000000000000000000
        //00000000000000000000000000000002
        //XXXXXXXX000000000000000000000000
        //00000000000000000000000000000000
        //XXXXXXXXXXXXXXXXXXXX000000000000
        //00000000000000000000000000000000

        Debug.Log(function.DecodeSimpleTypeOutput<int[]>(Request.Result));
       

    }

    public IEnumerator setServer(string nameServer, string ipAddress)
    {

        //comprobaciones
        //{.........}

        //.SOL bytes32
        string data = nameServer + _DELIMITERBTW + ipAddress + _DELIMITEREND;
        if (data.Length > _MAXLENSTRING)
        {
            Debug.LogError("Error Max 32 length");
            yield break;
        }


        HexBigInteger gas = new HexBigInteger(200000);
        HexBigInteger gasprice = new HexBigInteger(80);
        HexBigInteger valuegasprice = new HexBigInteger(0);

        var function = contract.GetFunction("SetServer");

        var transactionInput = function.CreateTransactionInput(accountAddress, gas, gasprice, valuegasprice, data);
        var transactionSignedRequest = new TransactionSignedUnityRequest(_url, accountPrivateKey, accountAddress);

        Debug.Log("SetServer transaction being submitted..");
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if (transactionSignedRequest.Exception == null)
        {
            Debug.Log("TX created: " + transactionSignedRequest.Result);

        }
        else
        {
            Debug.LogError("Error TX: " + transactionSignedRequest.Exception.Message);
        }


    }




    string HEX2ASCII(string hex)

    {

        string res = String.Empty;

        for (int a = 0; a < hex.Length; a = a + 2)

        {

            string Char2Convert = hex.Substring(a, 2);

            int n = Convert.ToInt32(Char2Convert, 16);

            char c = (char)n;

            res += c.ToString();

        }

        return res;

    }

    //ya tiene _delimiterend incluido
    public IEnumerator KillServer(string nameServer)
    {

        HexBigInteger gas = new HexBigInteger(80000);
        HexBigInteger gasprice = new HexBigInteger(100);
        HexBigInteger valuegasprice = new HexBigInteger(0);

        var function = contract.GetFunction("KillServer");
        var transactionInput = function.CreateTransactionInput(accountAddress, gas, gasprice, valuegasprice, nameServer);
        var transactionSignedRequest = new TransactionSignedUnityRequest(_url, accountPrivateKey, accountAddress);

        Debug.Log("KillServer transaction being submitted..");
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if (transactionSignedRequest.Exception == null)
        {
            Debug.Log("TX created: " + transactionSignedRequest.Result);

        }
        else
        {
            Debug.Log("Error TX: " + transactionSignedRequest.Exception.Message);
        }

       

    }

    void getBalanace()
    {

        StartCoroutine(getAccountBalance(accountAddress, (balance) => {
            Debug.Log("Account balance: " + balance);
        }));

    }


    IEnumerator getAccountBalance(string address, System.Action<decimal> callback)
    {
        var getBalanceRequest = new EthGetBalanceUnityRequest(_url);
        yield return getBalanceRequest.SendRequest(address, Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if (getBalanceRequest.Exception == null)
        {
            var balance = getBalanceRequest.Result.Value;
            callback(Nethereum.Util.UnitConversion.Convert.FromWei(balance, 18));
        }
        else
        {
            throw new System.InvalidOperationException("Get balance request failed");
        }

    }

    void deployEthereumContract()
    {
        Debug.Log("Deploying contract...");

        StartCoroutine(deployContract(ABI, byteCode, accountAddress, (result) => {
            Debug.Log("Result=" + result);
        }));

    }


    IEnumerator deployContract(string abi, string byteCode, string senderAddress, System.Action<string> callback)
    {
        var gas = new HexBigInteger(900000);
        var transactionInput = contractTransactionBuilder.BuildTransaction(abi, byteCode, senderAddress, gas, null);

        var transactionSignedRequest = new TransactionSignedUnityRequest(_url, accountPrivateKey, accountAddress);

        Debug.Log("Sending contract transaction...");
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if (transactionSignedRequest.Exception == null)
        {
            callback(transactionSignedRequest.Result);
        }
        else
        {
            throw new InvalidOperationException("Deploy contract tx failed:" + transactionSignedRequest.Exception.Message);
        }
    }

}



/*

ListServer.sol sin optimizar 
pragma solidity ^0.4.20;
contract ListServers {
    
 
    bytes32[] public servidores;
    uint256[] public currentsize;
    
    uint256 maxsize = 4;
 
    function SetServer(bytes32 server) external returns (bool)
    {
        
        servidores.push(server);
        currentsize.push(1);
        return true;
        
        
    }
    
    function SetSizePlusOne(bytes32 server) external returns(uint256)
    {
        for (uint i =0; i < servidores.length; i++)
        {
            
            if (compareStrings(servidores[i], server) == true)
            {
                
                if (currentsize[i] < maxsize)
                {
                    
                    currentsize[i] = currentsize[i] + 1;
                    return currentsize[i];
                }
                else
                {
                    return 0;    
                    
                }
                
            }
            
        }
        
        return 0;
        
        
    }
 
    function GetServers() external view returns(bytes32[])
    {
        return servidores;
    }
    
    function GetSizes() external view returns(uint256[])
    {
        
        return currentsize;
        
    }
    
    function KillServer(bytes32 server) external returns(bool)
    {
       
        for (uint i =0; i < servidores.length; i++)
        {
            
            
            if (compareStrings(servidores[i], server) == true)
            {
                //delete servidores[i];
                
                for (uint t = i; t<servidores.length-1; t++)
                {
                    servidores[t] = servidores[t+1];
                    currentsize[t] = currentsize[t+1];
                }
                delete servidores[servidores.length-1];
                delete currentsize[servidores.length-1];
                servidores.length--;
                currentsize.length--;
                
                return true;
                
            }
            
            
        }
        
        
        return false;
    }
    
    
   
    
    
    function compareStrings (bytes32 a, bytes32 b) internal pure returns (bool){
       return keccak256(a) == keccak256(b);
   }
 
    
}
     */
