﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Threading;
using Open.Nat;
using System.Net.Sockets;
using System.Threading.Tasks;



public class TokenNat : MonoBehaviour {

   
    [HideInInspector]
    public IPAddress publicIPAddress = null;

    [HideInInspector]
    public IPAddress privateIPAddress = null;

    [HideInInspector]
    public string nameMatch = "";
   
    public int privatePort = 8001;
    public int publicPort = 8001;

    public bool isLocalHost = false;
    
    Thread tcpListenerThread;


    // Use this for initialization
    void Start () {

       

        DontDestroyOnLoad(this.gameObject);
        Application.runInBackground = true;


        privateIPAddress = GetLocalIPAddress();


        var discoverer = new NatDiscoverer();
        var cts = new CancellationTokenSource();
        cts.CancelAfter(5000);

        NatDevice device = null;
       
        var t = discoverer.DiscoverDeviceAsync(PortMapper.Upnp, cts);


        t.ContinueWith(tt =>
        {
            device = tt.Result;
            device.GetExternalIPAsync()
                .ContinueWith(task =>
                {
                    publicIPAddress = task.Result;
                    Debug.Log("Local IP: " + privateIPAddress);
                    Debug.Log("Public IP Address: " + publicIPAddress);

                    device.CreatePortMapAsync(new Mapping(Protocol.Tcp, privatePort, publicPort, "Open.Nat (lifetime)"));
                    return device.CreatePortMapAsync(new Mapping(Protocol.Udp, privatePort, publicPort, "Open.Nat (lifetime)"));

                });
            
                

        });

        //Escuchamos en TCP por si chequean puerto abierto con metodo TCP
        tcpListenerThread = new Thread(() => ListenForMessages(privateIPAddress, privatePort));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();

        try
        {
            t.Wait();
        }
        catch (AggregateException e)
        {
            if (e.InnerException is NatDeviceNotFoundException)
            {
                Debug.Log("Not found. El router tiene que tener activado el upnp o pmp. si es pmp tiene que cambiarlo en 'discoverer.DiscoverDeviceAsync(PortMapper.Upnp'");
            }
        }
        



    }


    IPAddress GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());

        for (ushort i = 0; i < host.AddressList.Length; i++)
        {
            if (host.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
            {
                return host.AddressList[i];
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }


    public void ListenForMessages(IPAddress localAddr, int port)
    {
        TcpListener server = null;
        try
        {
            
            server = new TcpListener(localAddr, port);

            server.Start();

            Byte[] bytes = new Byte[256];
            String data = null;

            while (true)
            {
                Debug.Log("Waiting for a TCP connection... [por si comprueban puerto abierto por metodo TCP https://portscanner.standingtech.com/]");

                using (TcpClient client = server.AcceptTcpClient())
                {

                    Debug.Log("Connected!");

                    data = null;

                    NetworkStream stream = client.GetStream();

                    int i;

                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Debug.Log(String.Format("Received: {0}", data));

                    }
                }
            }
        }
        catch (SocketException e)
        {
            Debug.LogError(String.Format("SocketException: {0}", e));
        }
        finally
        {
            server.Stop();
        }
    }
}